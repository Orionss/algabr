from copyreg import constructor


class matrix():
    def __init__(self):
        self._arr = []

    def set_array(self, arr):
        self._arr = arr

    @staticmethod
    def from_array(arr):
        m = matrix()
        m.set_array(arr)
        return m

    def __mul__(self, other):
        